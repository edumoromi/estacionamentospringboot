package com.exercicio.estacionamento;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/veiculo")
public class VeiculoController {
	@Autowired
	private VeiculoService service;
	
	@GetMapping()
	public ResponseEntity<Iterable<Veiculo>> get(){
		return new ResponseEntity<>(service.getVeiculo(), HttpStatus.OK);
	}
	
	@GetMapping("/placa/{placa}")
	public ResponseEntity get(@PathVariable("placa") String placa){
		Optional<Veiculo> veiculo = service.getVeiculoByplaca(placa);
		System.out.println("ENTRO&U");
		if (veiculo.isPresent()){
			return ResponseEntity.ok(veiculo.get());
		}else {
			return ResponseEntity.notFound().build();
		}
	
	}
	
	@GetMapping("/tipo/{tipo}")
	public ResponseEntity gettipo(@PathVariable("tipo") String tipo){
		List<Veiculo> veiculo = service.getVeiculoBytipo_veiculo(tipo);
		if (veiculo.isEmpty()){
			return ResponseEntity.noContent().build();
		}else {
			return 	ResponseEntity.ok(veiculo);
		}
	
	}
	
	@PostMapping
	public String post(@RequestBody Veiculo veiculo) {
		Veiculo v = service.save(veiculo);
		return "Veiculo salvo com sucesso!" + v.getPlaca();
	}
	
	@PutMapping("/{placa}")
	public String put(@PathVariable("placa") String placa, @RequestBody Veiculo veiculo) {
		Veiculo v = service.update(veiculo, placa);
		return "Veiculo atualizado com sucesso!" + v.getPlaca();
	}
	@DeleteMapping("/{placa}")
	public String delete(@PathVariable("placa") String placa) {
		service.delete(placa);
		return "Carro deletado com sucesso!";
	}
}
