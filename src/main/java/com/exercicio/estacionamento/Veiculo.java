package com.exercicio.estacionamento;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.*;

@Entity
@Data

public class Veiculo {
	private int id_Estacionamento;
	private int id_Usuario;
	@Id
	@Column(name ="placa_Veiculo")
	private String placa;
	private String modelo;
	@Column(name ="tipo_Veiculo")
	private String tipo;
	
	public Veiculo() {}
	
	public Veiculo(int id_Estacionamento, int id_Usuario, String placa, String modelo, String tipo) {
		this.id_Estacionamento = id_Estacionamento;
		this.id_Usuario = id_Usuario;
		this.placa = placa;
		this.modelo = modelo;
		this.tipo = tipo;
	}
	public int getId_Estacionamento() {
		return id_Estacionamento;
	}
	public void setId_Estacionamento(int id_Estacionamento) {
		this.id_Estacionamento = id_Estacionamento;
	}
	public int getId_usuario() {
		return id_Usuario;
	}
	public void setId_usuario(int id_Usuario) {
		this.id_Usuario = id_Usuario;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getTipo_veiculo() {
		return tipo;
	}
	public void setTipo_veiculo(String tipo) {
		this.tipo = tipo;
	}
	
}
