package com.exercicio.estacionamento;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface VeiculoRepository extends CrudRepository<Veiculo, String> {

	Optional<Veiculo> findByplaca(String placa);

	void deleteByplaca(String placa);

	List<Veiculo> findBytipo(String tipo);
}
