package com.exercicio.estacionamento;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeiculoService {

	@Autowired
	private VeiculoRepository rep;
	
	public Iterable<Veiculo> getVeiculo(){
		return rep.findAll();
	}

	public Optional<Veiculo> getVeiculoByplaca(String placa) {
		return rep.findByplaca(placa);
	}

	public Veiculo save(Veiculo veiculo) {
		return rep.save(veiculo);
	}

	public Veiculo update(Veiculo veiculo, String placa) {
		Optional<Veiculo> optional = getVeiculoByplaca(placa);
		if(optional.isPresent()) {
			Veiculo db = optional.get();
			db.setId_Estacionamento(veiculo.getId_Estacionamento());
			db.setId_usuario(veiculo.getId_usuario());
			db.setModelo(veiculo.getModelo());
			db.setTipo_veiculo(veiculo.getTipo_veiculo());
			rep.save(db);
			return db;
		}else {
			throw new RuntimeException("Não foi possivel atualizar registro");
		}	
	}
	public void delete(String placa) {
		Optional<Veiculo> optional = getVeiculoByplaca(placa);
		if (optional.isPresent()){
			rep.deleteById(placa);
		}
	}

	public List<Veiculo> getVeiculoBytipo_veiculo(String tipo) {
		return rep.findBytipo(tipo);
	}
	
}
