package com.exercicio.estacionamento.api;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class IndexController {
	@GetMapping()
	public String get() {
		return "Get";
	}
	
	@PostMapping()
	public String post() {
		return "Post";
	}

	@PutMapping()
	public String put() {
		return "Put";
	}
	
	@DeleteMapping()
	public String delete() {
		return "Delete";
	}
	
	
	
	/*
	 * @GetMapping("/userInfo") public UserDetails userInfo(@AuthenticationPrincipal
	 * UserDetails user) { return user; }
	 */
}
