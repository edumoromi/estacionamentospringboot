package com.exercicio.estacionamento;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static junit.framework.TestCase.*;

@SpringBootTest(classes = Veiculo.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)

class EstacionamentoApplicationTests {
    @Autowired
    protected TestRestTemplate rest;

	@Test
	public void test() {
		Veiculo veiculo = new Veiculo();
		veiculo.setId_Estacionamento(1);
		veiculo.setId_usuario(1);
		veiculo.setModelo("Corolla");
		
		// Insert
		ResponseEntity response = rest.withBasicAuth("user","user").postForEntity("/api/veiculos/", veiculo, null);
		System.out.println(response);
		// Verifica se criou
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
	}
}