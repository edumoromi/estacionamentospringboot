use estacionamento;

#drop table Veiculo;
#drop table Usuario;
#drop table Estacionamento;

CREATE TABLE Estacionamento(
id_Estacionamento int NOT NULL,
nome_Estacionamento varchar(30),
vagas_Estacionamento int,
PRIMARY KEY (id_Estacionamento)
);


CREATE TABLE Veiculo(
id_Estacionamento int NOT NULL,
id_Usuario int NOT NULL,
placa_Veiculo varchar(9) NOT NULL,
modelo varchar(30),
tipo_Veiculo char,
PRIMARY KEY (placa_Veiculo)
);

CREATE TABLE Usuario(
id_Estacionamento int NOT NULL,
id_Usuario int AUTO_INCREMENT,
nome_Usuario varchar(30),
rg_Usuario varchar(50),
PRIMARY KEY (id_Usuario)
);

ALTER TABLE Veiculo ADD CONSTRAINT FK_estacionamento_veiculo
FOREIGN KEY (id_Estacionamento) REFERENCES Estacionamento(id_Estacionamento);

ALTER TABLE Veiculo ADD CONSTRAINT FK_usuario_veiculo
FOREIGN KEY (id_Usuario) REFERENCES Usuario(id_Usuario);

ALTER TABLE Usuario ADD CONSTRAINT FK_estacionamento_usuario
FOREIGN KEY (id_Estacionamento) REFERENCES Estacionamento(id_Estacionamento);




